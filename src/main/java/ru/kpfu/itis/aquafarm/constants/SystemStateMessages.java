package ru.kpfu.itis.aquafarm.constants;

public interface SystemStateMessages {

    interface Heater {
        String TURNED_ON = "Heater turned on successfully";
        String ALREADY_TURNED_ON = "Heater's already turned on";
        String TURNED_OFF = "Heater turned off successfully";
        String ALREADY_TURNED_OFF = "Heater's already turned off";
    }

    interface Vent {
        String TURNED_ON = "Vent turned on successfully";
        String ALREADY_TURNED_ON = "Vent's already turned on";
        String TURNED_OFF = "Vent turned off successfully";
        String ALREADY_TURNED_OFF = "Vent's already turned off";
    }

    interface Valve {
        String OPENED = "Valve opened successfully";
        String ALREADY_OPEN = "Valve's already open";
        String CLOSED = "Valve closed successfully";
        String ALREADY_CLOSED = "Valve's already closed";
    }

    interface OXYGEN {
        String INVALID_ATMOS = "Invalid atmos value. Please enter atmos from 0 to 4";
    }

    interface CARBON_DIOXIDE {
        String INVALID_POWER = "Invalid power value. Please enter power from 0 to 8";
    }
}
