package ru.kpfu.itis.aquafarm.controller.constants;

public interface Params {
    String FRESH_WATER = "fresh_water";
    String FRESH_WATER_TEMPERATURE = "fresh_water_temperature";
}
