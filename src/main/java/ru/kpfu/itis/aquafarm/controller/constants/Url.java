package ru.kpfu.itis.aquafarm.controller.constants;

public interface Url {

    String VERSION = "/v1";

    String GET_PARAMS = "/params";

    String GET_PARAMS_FANCY = GET_PARAMS + "/fancy";

    String ADD_FRESH_WATTER = "/add_fresh_water";

    String FISH_TANK = "/fish_tank";

    String FISH_TANK_WATER_LEVEL_DECREASE = FISH_TANK + "/open_valve";

    String STOP_FISH_TANK_WATER_LEVEL_DECREASE = FISH_TANK + "/close_valve";

    String TEMPERATURE = "/temperature";

    String INCREASE_AIR_TEMPERATURE = TEMPERATURE + "/heat_on";

    String STOP_INCREASE_AIR_TEMPERATURE = TEMPERATURE + "/heat_off";

    String DECREASE_AIR_TEMPERATURE = TEMPERATURE + "/vent_on";

    String STOP_DECREASE_AIR_TEMPERATURE = TEMPERATURE + "/vent_off";

    String OXYGEN_PRESSURE = "/oxygen_pressure";

    String OXYGEN_PRESSURE_MANUAL = OXYGEN_PRESSURE + "/manual";

    String CARBON_DIOXIDE_SETUP = "/carbon_dioxide";
}
