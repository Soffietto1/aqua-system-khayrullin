package ru.kpfu.itis.aquafarm.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.aquafarm.controller.constants.Params;
import ru.kpfu.itis.aquafarm.controller.constants.RequestDescription;
import ru.kpfu.itis.aquafarm.controller.constants.Url;
import ru.kpfu.itis.aquafarm.dto.ParamsViewDto;
import ru.kpfu.itis.aquafarm.service.AquaService;

import java.util.Map;

@Api(tags = "Aqua system")
@RequiredArgsConstructor
@RestController
@RequestMapping(Url.VERSION)
public class AquaSystemMainController {

    private final AquaService aquaService;

    @ApiOperation(value = RequestDescription.Main.GET_PARAMS)
    @GetMapping(Url.GET_PARAMS)
    public ParamsViewDto getParamsInfo() {
        return aquaService.getParams();
    }

    @ApiOperation(value = RequestDescription.Main.GET_PARAMS_FANCY)
    @GetMapping(Url.GET_PARAMS_FANCY)
    public Map<String, String> getParamsInfoFancy() {
        return aquaService.getFancyParams();
    }

    @ApiOperation(value = RequestDescription.Main.ADD_FRESH_WATTER)
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = Params.FRESH_WATER,
                    value = RequestDescription.FRESH_WATER,
                    paramType = "query",
                    required = true
            ),
            @ApiImplicitParam(
                    name = Params.FRESH_WATER_TEMPERATURE,
                    value = RequestDescription.FRESH_WATER_TEMPERATURE,
                    paramType = "query",
                    required = true
            )
    })
    @PostMapping(Url.ADD_FRESH_WATTER)
    public ParamsViewDto addFreshWater(
            @RequestParam(Params.FRESH_WATER) double freshWater,
            @RequestParam(Params.FRESH_WATER_TEMPERATURE) double freshWaterTemperature
    ) {
        return aquaService.increaseFishTankWaterLevel(freshWater, freshWaterTemperature);
    }

    @ApiOperation(value = RequestDescription.Main.FISH_TANK_WATER_LEVEL_DECREASE)
    @PostMapping(Url.FISH_TANK_WATER_LEVEL_DECREASE)
    public ResponseEntity<String> decreaseWaterTankLevel() {
        return ResponseEntity.ok(aquaService.decreaseFishTankWaterLevel());
    }

    @ApiOperation(value = RequestDescription.Main.STOP_FISH_TANK_WATER_LEVEL_DECREASE)
    @PostMapping(Url.STOP_FISH_TANK_WATER_LEVEL_DECREASE)
    public ResponseEntity<String> stopFishTankWaterLevelDecrease() {
        return ResponseEntity.ok(aquaService.stopFishTankWaterLevelDecrease());
    }

    @ApiOperation(value = RequestDescription.Main.INCREASE_AIR_TEMPERATURE)
    @PostMapping(Url.INCREASE_AIR_TEMPERATURE)
    public ResponseEntity<String> increaseAirTemperature() {
        return ResponseEntity.ok(aquaService.increaseAirTemperature());
    }

    @ApiOperation(value = RequestDescription.Main.STOP_INCREASE_AIR_TEMPERATURE)
    @PostMapping(Url.STOP_INCREASE_AIR_TEMPERATURE)
    public ResponseEntity<String> stopIncreaseAirTemperature() {
        return ResponseEntity.ok(aquaService.stopIncreaseAirTemperature());
    }

    @ApiOperation(value = RequestDescription.Main.DECREASE_AIR_TEMPERATURE)
    @PostMapping(Url.DECREASE_AIR_TEMPERATURE)
    public ResponseEntity<String> decreaseAirTemperature() {
        return ResponseEntity.ok(aquaService.decreaseAirTemperature());
    }

    @ApiOperation(value = RequestDescription.Main.STOP_DECREASE_AIR_TEMPERATURE)
    @PostMapping(Url.STOP_DECREASE_AIR_TEMPERATURE)
    public ResponseEntity<String> stopDecreaseAirTemperature() {
        return ResponseEntity.ok(aquaService.stopDecreaseAirTemperature());
    }

    @ApiOperation(value = RequestDescription.Main.OXYGEN_ENRICHMENT_PRESSURE_SETUP)
    @PostMapping(Url.OXYGEN_PRESSURE_MANUAL)
    public ParamsViewDto oxygenEnrichmentPressureSetup(@RequestParam int atmos) {
        return aquaService.oxygenEnrichmentPressureSetupManual(atmos);
    }

    @ApiOperation(value = RequestDescription.Main.CARBON_DIOXIDE_LEVEL_SETUP)
    @PostMapping(Url.CARBON_DIOXIDE_SETUP)
    public ParamsViewDto carbonDioxideRemovalSetup(@RequestParam int power) {
        return aquaService.carbonDioxideRemovalSetup(power);
    }
}
