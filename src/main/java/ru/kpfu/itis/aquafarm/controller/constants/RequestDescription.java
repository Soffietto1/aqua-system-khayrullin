package ru.kpfu.itis.aquafarm.controller.constants;

public interface RequestDescription {

    String FRESH_WATER = "Уровень пресной воды";
    String FRESH_WATER_TEMPERATURE = "Температура пресной воды";

    interface Main {
        String GET_PARAMS = "Получение параметров системы";
        String GET_PARAMS_FANCY = "Получение параметров системы в красивом формате";
        String ADD_FRESH_WATTER = "Увеличение уровня воды в резервуаре для рыб (R1) с помощью добавления пресной воды";
        String FISH_TANK_WATER_LEVEL_DECREASE = "Уменьшение уровня воды в резервуаре для рыб (R1) с помощью открытия клапана";
        String STOP_FISH_TANK_WATER_LEVEL_DECREASE = "Остановка уменьшение уровня воды в резервуаре для рыб (R1) с помощью закрытия клапана";
        String INCREASE_AIR_TEMPERATURE = "Увеличение температуры воздуха в помещении с помощью включения системы по нагреву температуры воздуха";
        String STOP_INCREASE_AIR_TEMPERATURE = "Остановка увеличения температуры воздуха в помещении с помощью включения системы по нагреву температуры воздуха";
        String DECREASE_AIR_TEMPERATURE = "Увеличение температуры воздуха в помещении с помощью включения системы по нагреву температуры воздуха";
        String STOP_DECREASE_AIR_TEMPERATURE = "Остановка уменьшения температуры воздуха с помощью отключения вентиляционной системы";
        String OXYGEN_ENRICHMENT_PRESSURE_SETUP = "Настройка мощности установки по обогащению кислородом в резервуаре для рыб";
        String CARBON_DIOXIDE_LEVEL_SETUP = "Настройка удаления из воды углекислого газа";
    }
}
