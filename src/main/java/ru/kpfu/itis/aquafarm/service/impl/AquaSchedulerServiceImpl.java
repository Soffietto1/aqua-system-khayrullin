package ru.kpfu.itis.aquafarm.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.aquafarm.enums.HeaterState;
import ru.kpfu.itis.aquafarm.enums.ValveState;
import ru.kpfu.itis.aquafarm.enums.VentState;
import ru.kpfu.itis.aquafarm.exceptions.FishDiedException;
import ru.kpfu.itis.aquafarm.service.AquaSchedulerService;
import ru.kpfu.itis.aquafarm.util.ParamDataHolderInMemory;

@Slf4j
@Component
@RequiredArgsConstructor
public class AquaSchedulerServiceImpl implements AquaSchedulerService {

    private final ParamDataHolderInMemory paramDataHolderInMemory;

    private ValveState valveState = ValveState.CLOSED;

    private HeaterState heaterState = HeaterState.OFF;

    private VentState ventState = VentState.OFF;

    private double oldAirTemperature = 24;

    @Override
    public void openValve() {
        this.valveState = ValveState.OPEN;
    }

    @Override
    public void closeValve() {
        this.valveState = ValveState.CLOSED;
    }

    @Override
    public void turnHeaterOn() {
        this.heaterState = HeaterState.ON;
    }

    @Override
    public void turnHeaterOff() {
        this.heaterState = HeaterState.OFF;
    }

    @Override
    public void turnVentOn() {
        this.ventState = VentState.ON;
    }

    @Override
    public void turnVentOff() {
        this.ventState = VentState.OFF;
    }

    @Scheduled(fixedDelay = 1000)
    private void decreaseWaterLevel() {
        if (ValveState.isOpen(valveState)) {
            double currentWaterLevel = paramDataHolderInMemory.getWaterVolume();
            double newWaterLevel = currentWaterLevel - 10;

            paramDataHolderInMemory.setWaterVolume(newWaterLevel);
        }
    }

    @Scheduled(fixedDelay = 1000)
    private void increaseTemperature() {
        if (HeaterState.isOn(heaterState)) {
            double currentAirTemperature = paramDataHolderInMemory.getIndoorTemperature();
            double newAirTemperature = currentAirTemperature + 0.1;
            double temperatureDifference = Math.abs(oldAirTemperature - newAirTemperature);

            double percent = getPercent(currentAirTemperature);

            double currentIndoorHumidity = paramDataHolderInMemory.getIndoorHumidity();
            double newIndoorHumidity = currentIndoorHumidity + percent * temperatureDifference;

            paramDataHolderInMemory.setIndoorTemperature(newAirTemperature);
            paramDataHolderInMemory.setIndoorHumidity(newIndoorHumidity);
        }
        oldAirTemperature = paramDataHolderInMemory.getIndoorTemperature();
    }

    @Scheduled(fixedDelay = 1000)
    private void decreaseTemperature() {
        if (VentState.isOn(ventState)) {
            double currentAirTemperature = paramDataHolderInMemory.getIndoorTemperature();
            double newAirTemperature = currentAirTemperature - 0.1;
            double temperatureDifference = Math.abs(oldAirTemperature - newAirTemperature);

            double percent = getPercent(currentAirTemperature);


            double currentIndoorHumidity = paramDataHolderInMemory.getIndoorHumidity();
            double newIndoorHumidity = currentIndoorHumidity - percent * temperatureDifference;

            paramDataHolderInMemory.setIndoorTemperature(newAirTemperature);
            paramDataHolderInMemory.setIndoorHumidity(newIndoorHumidity);
        }
        oldAirTemperature = paramDataHolderInMemory.getIndoorTemperature();
    }

    @Scheduled(fixedDelay = 1000)
    private void checkSystemStatus() {
        if (paramDataHolderInMemory.getIndoorHumidity() > 100) {
            heaterState = HeaterState.OFF;
            paramDataHolderInMemory.setIndoorHumidity(100);
            paramDataHolderInMemory.setPlantsCondition(0);
            log.error("Humidity is 100%. Plants died. Heater turned off automaticaly");
        }
        if (paramDataHolderInMemory.getIndoorHumidity() < 0) {
            heaterState = HeaterState.OFF;
            paramDataHolderInMemory.setIndoorHumidity(0);
            paramDataHolderInMemory.setPlantsCondition(0);
            log.error("Humidity is 0%. Plants died. Heater turned off automaticaly");
        }
        if (paramDataHolderInMemory.getWaterVolume() < 0) {
            valveState = ValveState.CLOSED;
            paramDataHolderInMemory.setWaterVolume(0);
            paramDataHolderInMemory.setFishWeight(0);
            log.error("Water volume = 0. Fish died. Heater turned off automaticaly");
        }
        if (paramDataHolderInMemory.getAquaSystemTemperature() > 50) {
            paramDataHolderInMemory.setAquaSystemTemperature(50);
            paramDataHolderInMemory.setFishWeight(0);
            log.error("Aqua System Temperature = 50. Fish died");
        }
        if (paramDataHolderInMemory.getAquaSystemTemperature() < 0) {
            paramDataHolderInMemory.setAquaSystemTemperature(0);
            paramDataHolderInMemory.setFishWeight(0);
            log.error("Aqua System Temperature = 0. Fish died");
        }
    }

    private double getPercent(double currentAirTemperature) {
        if (currentAirTemperature >= 0 && currentAirTemperature < 5) {
            return 1;
        } else if (currentAirTemperature >= 5 && currentAirTemperature < 10) {
            return 1.5;
        } else if (currentAirTemperature >= 10 && currentAirTemperature < 15) {
            return 2;
        } else if (currentAirTemperature >= 15 && currentAirTemperature < 20) {
            return 2.5;
        } else if (currentAirTemperature >= 20 && currentAirTemperature < 25) {
            return 3;
        } else if (currentAirTemperature >= 25 && currentAirTemperature < 30) {
            return 3.5;
        } else if (currentAirTemperature >= 30) {
            return 4;
        } else {
            throw new FishDiedException("Air temperature is below 0");
        }
    }

    @Override
    public ValveState getValveState() {
        return valveState;
    }

    @Override
    public HeaterState getHeaterState() {
        return heaterState;
    }

    @Override
    public VentState getVentState() {
        return ventState;
    }
}
