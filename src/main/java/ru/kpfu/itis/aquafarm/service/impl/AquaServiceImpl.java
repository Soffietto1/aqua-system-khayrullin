package ru.kpfu.itis.aquafarm.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.aquafarm.constants.SystemStateMessages;
import ru.kpfu.itis.aquafarm.dto.ParamsViewDto;
import ru.kpfu.itis.aquafarm.enums.HeaterState;
import ru.kpfu.itis.aquafarm.enums.ValveState;
import ru.kpfu.itis.aquafarm.enums.VentState;
import ru.kpfu.itis.aquafarm.exceptions.StateException;
import ru.kpfu.itis.aquafarm.mapper.ParamsMapper;
import ru.kpfu.itis.aquafarm.service.AquaSchedulerService;
import ru.kpfu.itis.aquafarm.service.AquaService;
import ru.kpfu.itis.aquafarm.util.ParamDataHolderInMemory;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

@Slf4j
@Service
@RequiredArgsConstructor
public class AquaServiceImpl implements AquaService {

    private static final Random RANDOM = new Random();

    private final ParamDataHolderInMemory paramDataHolderInMemory;

    private final ParamsMapper paramsMapper;

    private final AquaSchedulerService aquaSchedulerService;

    @Override
    public ParamsViewDto increaseFishTankWaterLevel(double freshWater, double freshWaterTemperature) {
        double currentFishTanksWaterLevel = paramDataHolderInMemory.getFishTanksWaterLevel();
        double currentPhLevel = paramDataHolderInMemory.getPhLevel();
        double currentAquaSystemTemperature = paramDataHolderInMemory.getAquaSystemTemperature();
        double currentCarbonDioxideLevel = paramDataHolderInMemory.getCarbonDioxideLevel();
        double currentOxygenLevel = paramDataHolderInMemory.getOxygenLevel();

        double fishTanksWaterLevelIncrease = getFishTanksWaterLevelIncrease(freshWater);
        double newPhLevel = getNewPhLevel(fishTanksWaterLevelIncrease, currentFishTanksWaterLevel, currentPhLevel);
        double newWaterTankTemperature = getNewWaterTankTemperature(
                currentAquaSystemTemperature,
                currentFishTanksWaterLevel,
                fishTanksWaterLevelIncrease,
                freshWaterTemperature
        );
        double newFishTankWaterLevel = getNewFishTankWaterLevel(currentFishTanksWaterLevel, fishTanksWaterLevelIncrease);
        double newCarbonDioxideLevel = getNewCarbonDioxideLevel(
                fishTanksWaterLevelIncrease,
                currentFishTanksWaterLevel,
                currentCarbonDioxideLevel
        );
        double newOxygenLevel = getNewOxygenLevel(fishTanksWaterLevelIncrease,
                currentFishTanksWaterLevel,
                currentOxygenLevel);
        double newWaterVolume = getNewWaterVolume(newFishTankWaterLevel);

        paramDataHolderInMemory
                .setPhLevel(newPhLevel)
                .setAquaSystemTemperature(newWaterTankTemperature)
                .setFishTanksWaterLevel(newFishTankWaterLevel)
                .setCarbonDioxideLevel(newCarbonDioxideLevel)
                .setOxygenLevel(newOxygenLevel)
                .setWaterVolume(newWaterVolume);

        return paramsViewDto();
    }

    @Override
    public String decreaseFishTankWaterLevel() {
        if (ValveState.isOpen(aquaSchedulerService.getValveState())) {
            throw new StateException(SystemStateMessages.Valve.ALREADY_OPEN);
        }
        aquaSchedulerService.openValve();
        return SystemStateMessages.Valve.OPENED;
    }

    @Override
    public String stopFishTankWaterLevelDecrease() {
        if (ValveState.isClosed(aquaSchedulerService.getValveState())) {
            throw new StateException(SystemStateMessages.Valve.ALREADY_CLOSED);
        }
        aquaSchedulerService.closeValve();
        return SystemStateMessages.Valve.CLOSED;
    }

    @Override
    public String increaseAirTemperature() {
        if (HeaterState.isOn(aquaSchedulerService.getHeaterState())) {
            throw new StateException(SystemStateMessages.Heater.ALREADY_TURNED_ON);
        }
        aquaSchedulerService.turnHeaterOn();
        return SystemStateMessages.Heater.TURNED_ON;
    }

    @Override
    public String stopIncreaseAirTemperature() {
        if (HeaterState.isOff(aquaSchedulerService.getHeaterState())) {
            throw new StateException(SystemStateMessages.Heater.ALREADY_TURNED_OFF);
        }
        aquaSchedulerService.turnHeaterOff();
        return SystemStateMessages.Heater.TURNED_OFF;
    }

    @Override
    public String decreaseAirTemperature() {
        if (VentState.isOn(aquaSchedulerService.getVentState())) {
            throw new StateException(SystemStateMessages.Vent.ALREADY_TURNED_ON);
        }
        aquaSchedulerService.turnVentOn();
        return SystemStateMessages.Vent.TURNED_ON;
    }

    @Override
    public String stopDecreaseAirTemperature() {
        if (VentState.isOff(aquaSchedulerService.getVentState())) {
            throw new StateException(SystemStateMessages.Vent.ALREADY_TURNED_OFF);
        }
        aquaSchedulerService.turnVentOff();
        return SystemStateMessages.Vent.TURNED_OFF;
    }

    @Override
    public ParamsViewDto oxygenEnrichmentPressureSetupManual(int atmos) {
        if (atmos < 0 || atmos > 4) {
            throw new StateException(SystemStateMessages.OXYGEN.INVALID_ATMOS);
        }
        double oxygenLevel = paramDataHolderInMemory.getOxygenLevel();
        double oxygenEnrichmentPressure = paramDataHolderInMemory.getOxygenEnrichmentPressure();
        double newOxygenLevel = oxygenLevel + atmos / 60d;
        paramDataHolderInMemory
                .setOxygenLevel(newOxygenLevel)
                .setOxygenEnrichmentPressure(oxygenEnrichmentPressure + atmos * 0.01);
        return getParams();
    }

    @Override
    public ParamsViewDto carbonDioxideRemovalSetup(int power) {
        if (power < 0 || power > 8) {
            throw new StateException(SystemStateMessages.CARBON_DIOXIDE.INVALID_POWER);
        }
        double carbonDioxideLevel = paramDataHolderInMemory.getCarbonDioxideLevel();
        double newCarbonDioxideLevel = carbonDioxideLevel - power / 60d;
        paramDataHolderInMemory
                .setCarbonDioxideLevel(newCarbonDioxideLevel);
        return getParams();
    }

    @Override
    public Map<String, String> getFancyParams() {
        ParamsViewDto paramsViewDto = paramsViewDto();
        DecimalFormat df2 = new DecimalFormat("#.##");

        Map<String, String> params = new LinkedHashMap<>();
        params.put("Fish Weigh", df2.format(paramsViewDto.getFishWeight()) + " " + "kg");
        params.put("Plants Condition", df2.format(paramsViewDto.getPlantsCondition()) + " " + "%");
        params.put("Water Volume", df2.format(paramsViewDto.getWaterVolume()) + " " + "lit");
        params.put("Oxygen Enrichment Pressure", df2.format(paramsViewDto.getOxygenEnrichmentPressure()) + " " + "atm");
        params.put("Carbon Dioxide Removal Power", df2.format(paramsViewDto.getCarbonDioxideRemovalPower()) + " " + "mg/lit/min");
        params.put("Pump Clock Frequency", df2.format(paramsViewDto.getPumpClockFrequency()) + " " + "gc");
        params.put("Filter Cleaning Frequency", df2.format(paramsViewDto.getFilterCleaningFrequency()) + " " + "gc");
        params.put("Indoor Temperature", df2.format(paramsViewDto.getIndoorTemperature()) + " " + "°С");
        params.put("Fish Tanks Water Level", df2.format(paramsViewDto.getFishTanksWaterLevel()) + " " + "m");
        params.put("Indoor Humidity", df2.format(paramsViewDto.getIndoorHumidity()) + " " + "%");
        params.put("Ph Level", df2.format(paramsViewDto.getPhLevel()) + " " + "pH");
        params.put("Oxygen Level", df2.format(paramsViewDto.getOxygenLevel()) + " " + "mg/lit");
        params.put("Carbon Dioxide Level", df2.format(paramsViewDto.getCarbonDioxideLevel()) + " " + "mg/lit");
        params.put("Aqua System Temperature", df2.format(paramsViewDto.getAquaSystemTemperature()) + " " + "°С");
        params.put("In/Out Filter Water Pressure", df2.format(paramsViewDto.getInOutFilterWaterPressure()) + " " + "atm");
        params.put("Pump Current", df2.format(paramsViewDto.getPumpCurrent()) + " " + "A");
        params.put("Sump Water Level", df2.format(paramsViewDto.getSumpWaterLevel()) + " " + "m");
        params.put("Mechanical Filter Water Level", df2.format(paramsViewDto.getMechanicalFilterWaterLevel()) + " " + "m");
        params.put("Biological Filter Water Level", df2.format(paramsViewDto.getBiologicalFilterWaterLevel()) + " " + "m");
        params.put("Plant Space Water Level", df2.format(paramsViewDto.getPlantSpaceWaterLevel()) + " " + "m");
        params.put("Carbon Dioxide Removal Water Level", df2.format(paramsViewDto.getCarbonDioxideRemovalWaterLevel()) + " " + "m");
        params.put("Nitrogen Level", df2.format(paramsViewDto.getNitrogenLevel()) + " " + "mg/lit");
        params.put("Ammonia Level", df2.format(paramsViewDto.getAmmoniaLevel()) + " " + "mg/lit");
        params.put("Nitrates Level", df2.format(paramsViewDto.getNitratesLevel()) + " " + "mg/lit");
        params.put("Nitrites Level", df2.format(paramsViewDto.getNitritesLevel()) + " " + "mg/lit");
        params.put("Water Flowing Speed", df2.format(paramsViewDto.getWaterFlowingSpeed()) + " " + "m/sec");
        params.put("Heater State", aquaSchedulerService.getHeaterState().toString());
        params.put("Valve State", aquaSchedulerService.getValveState().toString());
        params.put("Vent State", aquaSchedulerService.getVentState().toString());

        return params;
    }

    @Override
    public ParamsViewDto getParams() {
        return paramsViewDto();
    }

    /**
     * @param freshWater уровень добавляемой прессной воды в резервуар для рыб
     * @return Уровень воды, на который увеличится уровень воды в резервуаре для рыб
     */
    private double getFishTanksWaterLevelIncrease(double freshWater) {
        return freshWater * 0.02;
    }

    /**
     * @param fishTankWaterLevelIncrease добавляемый уровень воды в резервуар для рыб
     * @param currentFishTankWaterLevel  текущий уровень воды в резервуаре для рыб
     * @param currentPhLevel             текущий уровень rn
     * @return Изменения pH в резервуаре для рыб при добавлении воды в резервуар для рыб
     */
    private double getNewPhLevel(double fishTankWaterLevelIncrease,
                                 double currentFishTankWaterLevel,
                                 double currentPhLevel) {
        double phNew = RANDOM.nextInt((100 - 55) + 1) + 55 / 10d;
        return (currentPhLevel * currentFishTankWaterLevel + phNew * fishTankWaterLevelIncrease)
                / (fishTankWaterLevelIncrease + currentFishTankWaterLevel);
    }

    /**
     * @param currentTemperature         текущая температура
     * @param currentFishTankWaterLevel  текущий уровень воды в резервуаре для рыб
     * @param fishTankWaterLevelIncrease добавляемый уровень воды в резервуар для рыб
     * @return Получить Изменение температуры воды в резервуаре для рыб после добавления пресной воды
     */
    private double getNewWaterTankTemperature(double currentTemperature,
                                              double currentFishTankWaterLevel,
                                              double fishTankWaterLevelIncrease,
                                              double freshWaterTemperature) {
        return (currentTemperature * currentFishTankWaterLevel + freshWaterTemperature * fishTankWaterLevelIncrease)
                / (fishTankWaterLevelIncrease + currentFishTankWaterLevel);
    }


    /**
     * @param currentFishTankWaterLevel  текущий уровень воды в резервуаре для рыб
     * @param fishTankWaterLevelIncrease добавляемый уровень воды в резервуар для рыб
     * @return Получить изменение уровня воды в резервуаре для рыб после добавление новой пресной воды в резервуар для рыб
     */
    private double getNewFishTankWaterLevel(double currentFishTankWaterLevel,
                                            double fishTankWaterLevelIncrease) {
        return currentFishTankWaterLevel + fishTankWaterLevelIncrease;
    }

    /**
     * @param fishTankWaterLevelIncrease добавляемый уровень воды в резервуар для рыб
     * @param currentFishTankWaterLevel  текущий уровень воды в резервуаре для рыб
     * @param currentCarbonDioxideLevel  текущий уровень углекислово газа
     * @return Изменение CO2 в резервуаре для рыб после добавление пресной воды
     */
    private double getNewCarbonDioxideLevel(double fishTankWaterLevelIncrease,
                                            double currentFishTankWaterLevel,
                                            double currentCarbonDioxideLevel) {
        double co2New = 26;
        return (currentCarbonDioxideLevel * currentFishTankWaterLevel + fishTankWaterLevelIncrease * co2New)
                / (fishTankWaterLevelIncrease + currentFishTankWaterLevel);
    }

    /**
     * @param fishTankWaterLevelIncrease добавляемый уровень воды в резервуар для рыб
     * @param currentFishTankWaterLevel  текущий уровень воды в резервуаре для рыб
     * @param currentOxygenLevel         текущий уровень кислорода
     * @return Изменение O2 в резервуаре для рыб после добавление пресной воды
     */
    private double getNewOxygenLevel(double fishTankWaterLevelIncrease,
                                     double currentFishTankWaterLevel,
                                     double currentOxygenLevel) {
        double o2New = RANDOM.nextInt((60 - 45) + 1) + 45 / 10d;
        return (currentOxygenLevel * currentFishTankWaterLevel + o2New * fishTankWaterLevelIncrease)
                / (fishTankWaterLevelIncrease + currentFishTankWaterLevel);
    }

    /**
     * @param newFishTankWaterLevel новый уровень воды
     * @return Измененный объем воды в системе
     */
    private double getNewWaterVolume(double newFishTankWaterLevel) {
        return newFishTankWaterLevel * 1000;
    }

    private ParamsViewDto paramsViewDto() {
        return paramsMapper.toViewDto(paramDataHolderInMemory);
    }
}
