package ru.kpfu.itis.aquafarm.service;

import ru.kpfu.itis.aquafarm.dto.ParamsViewDto;

import java.util.Map;

public interface AquaService {

    /**
     * @param freshWater уровень добавляемой прессной воды в резервуар для рыб
     *
     * Увеличение уровня воды в резервуаре для рыб (R1) с помощью добавления пресной воды
     */
    ParamsViewDto increaseFishTankWaterLevel(double freshWater, double freshWaterTemperature);


    /**
     * Уменьшение уровня воды в резервуаре для рыб (R1) с помощью открытия клапана
     */
    String decreaseFishTankWaterLevel();


    /**
     *  Остановка уменьшение уровня воды в резервуаре для рыб (R1) с помощью закрытия клапана
     */
    String stopFishTankWaterLevelDecrease();

    /**
     * Увеличение температуры воздуха в помещении с помощью включения системы по нагреву температуры воздуха
     */
    String increaseAirTemperature();

    /**
     * Остановка увеличения температуры воздуха в помещении с помощью включения системы по нагреву температуры воздуха
     */
    String stopIncreaseAirTemperature();

    /**
     * Уменьшение температуры воздуха с помощью включения вентиляционной системы
     */
    String decreaseAirTemperature();

    /**
     * Остановка уменьшения температуры воздуха с помощью включения вентиляционной системы
     */
    String stopDecreaseAirTemperature();

    /**
     * Настройка мощности установки по обогащению кислородом в резервуаре для рыб
     */
    ParamsViewDto oxygenEnrichmentPressureSetupManual(int atmos);

    /**
     * Настройка удаления из воды углекислого газа
     */
    ParamsViewDto carbonDioxideRemovalSetup(int power);

    /**
     * @return Текущие параметры системы
     */
    Map<String, String> getFancyParams();

    ParamsViewDto getParams();

}
