package ru.kpfu.itis.aquafarm.service;

import ru.kpfu.itis.aquafarm.enums.HeaterState;
import ru.kpfu.itis.aquafarm.enums.ValveState;
import ru.kpfu.itis.aquafarm.enums.VentState;

public interface AquaSchedulerService {

    void openValve();

    void closeValve();

    void turnHeaterOn();

    void turnHeaterOff();

    void turnVentOn();

    void turnVentOff();

    ValveState getValveState();

    HeaterState getHeaterState();

    VentState getVentState();

}
