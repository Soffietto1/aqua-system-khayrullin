package ru.kpfu.itis.aquafarm.exceptions;

public class StateException extends RuntimeException {

    public StateException(String message) {
        super(message);
    }
}
