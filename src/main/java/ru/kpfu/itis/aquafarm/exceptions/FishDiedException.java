package ru.kpfu.itis.aquafarm.exceptions;

public class FishDiedException extends RuntimeException {
    private static final String MESSAGE = "Fishes died. Reason: %s";

    public FishDiedException(String message) {
        super(String.format(MESSAGE, message));
    }
}
