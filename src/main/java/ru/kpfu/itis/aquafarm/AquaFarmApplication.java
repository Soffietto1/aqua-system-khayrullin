package ru.kpfu.itis.aquafarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
public class AquaFarmApplication {


    public static void main(String[] args) {
        SpringApplication.run(AquaFarmApplication.class, args);
    }

}
