package ru.kpfu.itis.aquafarm.util;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@Accessors(chain = true)
public class ParamDataHolderInMemory {

    //Кол-во рыбы (в килограммах)
    private double fishWeight = 350;
    //Уровень жизни растений (в процентах)
    private double plantsCondition = 100;
    //Общий объем воды в аквапонной системе (в литрах)
    private double waterVolume = 1000;
    //Давление установки по обогащению воды кислородом (в атмосферах)
    private double oxygenEnrichmentPressure = 0;
    //Мощность работы установки по удалению из воды углекислого газа (мг/л/мин)
    private double carbonDioxideRemovalPower = 0;
    //Тактовая частота тока на насос (Гц)
    private double pumpClockFrequency = 50;
    //Тактовая частота фильтра механической очистки (Гц)
    private double filterCleaningFrequency = 50;
    //Температура воздуха в помещении (°С)
    private double indoorTemperature = 24;
    //Уровень воды в резервуарах для рыб (м)
    private double fishTanksWaterLevel = 1;
    //Влажность воздуха в помещении (%)
    private double indoorHumidity = 50;
    //Уровень pH в воде аквапонной системы
    private double phLevel = 7;
    //Уровень растворенного в воде кислорода (мг/л)
    private double oxygenLevel = 8;
    //Уровень углекислого газа в воде аквапонной системы (мг/л)
    private double carbonDioxideLevel = 5;
    //Температура воды в аквапонной системе (°С)
    private double aquaSystemTemperature = 24;
    //Давление воды на входе и выходе фильтра (атмосфера)
    private double inOutFilterWaterPressure = 1;
    //Сила тока на насос (А)
    private double pumpCurrent = 5;
    //Уровень воды в отстойнике (м)
    private double sumpWaterLevel = 0.8;
    //Уровень воды в фильтре механической очистки (м)
    private double mechanicalFilterWaterLevel = 0.6;
    //Уровень воды в фильтре биологической очистки (м)
    private double biologicalFilterWaterLevel = 0.7;
    //Уровень воды в емкости для растений (м)
    private double plantSpaceWaterLevel = 0.233;
    //Уровень воды в установке по удалению углекислого газа (м)
    private double carbonDioxideRemovalWaterLevel = 0.9;
    //Уровень азота в воде (мг/л)
    private double nitrogenLevel = 0.6;
    //Уровень аммиака в воде (мг/л)
    private double ammoniaLevel = 0.5;
    //Уровень нитратов в воде (мг/л)
    private double nitratesLevel = 80;
    //Уровень нитритов в воде (мг/л)
    private double nitritesLevel = 0.5;
    //Скорость движение воды (м/с)
    private double waterFlowingSpeed = 1.2;
}
