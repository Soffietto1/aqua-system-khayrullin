package ru.kpfu.itis.aquafarm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "Параметры аква системы")
@Setter
@Getter
public class ParamsViewDto {

    @ApiModelProperty(value = "Кол-во рыбы (кг)")
    private double fishWeight;

    @ApiModelProperty(value = "Уровень жизни растений (%)")
    private double plantsCondition;

    @ApiModelProperty(value = "Общий объем воды в аквапонной системе (л)")
    private double waterVolume;

    @ApiModelProperty(value = "Давление установки по обогащению воды кислородом (атмосфер)")
    private double oxygenEnrichmentPressure;

    @ApiModelProperty(value = "Мощность работы установки по удалению из воды углекислого газа (мг/л/мин)")
    private double carbonDioxideRemovalPower;

    @ApiModelProperty(value = "Тактовая частота тока на насос (Гц)")
    private double pumpClockFrequency;

    @ApiModelProperty(value = "Тактовая частота фильтра механической очистки (Гц)")
    private double filterCleaningFrequency;

    @ApiModelProperty(value = "Температура воздуха в помещении (°С)")
    private double indoorTemperature;

    @ApiModelProperty(value = "Уровень воды в резервуарах для рыб (м)")
    private double fishTanksWaterLevel;

    @ApiModelProperty(value = "Влажность воздуха в помещении (%)")
    private double indoorHumidity;

    @ApiModelProperty(value = "Уровень pH в воде аквапонной системы")
    private double phLevel;

    @ApiModelProperty(value = "Уровень растворенного в воде кислорода (мг/л)")
    private double oxygenLevel;

    @ApiModelProperty(value = "Уровень углекислого газа в воде аквапонной системы (мг/л)")
    private double carbonDioxideLevel;

    @ApiModelProperty(value = "Температура воды в аквапонной системе (°С)")
    private double aquaSystemTemperature;

    @ApiModelProperty(value = "Давление воды на входе и выходе фильтра (атмосфера)")
    private double inOutFilterWaterPressure;

    @ApiModelProperty(value = "Сила тока на насос (А)")
    private double pumpCurrent;

    @ApiModelProperty(value = "Уровень воды в отстойнике (м)")
    private double sumpWaterLevel;

    @ApiModelProperty(value = "Уровень воды в фильтре механической очистки (м)")
    private double mechanicalFilterWaterLevel;

    @ApiModelProperty(value = "Уровень воды в фильтре биологической очистки (м)")
    private double biologicalFilterWaterLevel;

    @ApiModelProperty(value = "Уровень воды в емкости для растений (м)")
    private double plantSpaceWaterLevel;

    @ApiModelProperty(value = "Уровень воды в установке по удалению углекислого газа (м)")
    private double carbonDioxideRemovalWaterLevel;

    @ApiModelProperty(value = "Уровень азота в воде (мг/л)")
    private double nitrogenLevel;

    @ApiModelProperty(value = "Уровень аммиака в воде (мг/л)")
    private double ammoniaLevel;

    @ApiModelProperty(value = "Уровень нитратов в воде (мг/л)")
    private double nitratesLevel;

    @ApiModelProperty(value = "Уровень нитритов в воде (мг/л)")
    private double nitritesLevel;

    @ApiModelProperty(value = "Скорость движение воды (м/с)")
    private double waterFlowingSpeed;
}
