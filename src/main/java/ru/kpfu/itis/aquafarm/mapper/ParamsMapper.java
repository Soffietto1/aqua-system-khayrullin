package ru.kpfu.itis.aquafarm.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.kpfu.itis.aquafarm.dto.ParamsViewDto;
import ru.kpfu.itis.aquafarm.util.ParamDataHolderInMemory;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ParamsMapper {

    ParamsViewDto toViewDto(ParamDataHolderInMemory params);
}
