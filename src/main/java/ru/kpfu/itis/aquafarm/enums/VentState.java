package ru.kpfu.itis.aquafarm.enums;

public enum VentState {
    ON,
    OFF;

    public static boolean isOn(VentState ventState) {
        return ventState == ON;
    }

    public static boolean isOff(VentState ventState) {
        return ventState == OFF;
    }
}
