package ru.kpfu.itis.aquafarm.enums;

public enum HeaterState {
    ON,
    OFF;

    public static boolean isOn(HeaterState heaterState) {
        return heaterState == ON;
    }

    public static boolean isOff(HeaterState heaterState) {
        return heaterState == OFF;
    }
}
