package ru.kpfu.itis.aquafarm.enums;

public enum ValveState {
    OPEN,
    CLOSED;

    public static boolean isOpen(ValveState valveState) {
        return valveState == OPEN;
    }

    public static boolean isClosed(ValveState valveState) {
        return valveState == CLOSED;
    }
}
